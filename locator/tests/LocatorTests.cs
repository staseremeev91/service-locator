﻿using Core.IOC;
using System;

namespace service_locator.locator.tests
{
    public class LocatorTests
    {
        public void Start()
        {
            var locator = new Locator();
            var instance = locator.Provide<ITest>();
            if (instance != null)
                instance.Print();
            //for (var i = 0; i < 5; i++)
            //{
            //    var instance = locator.Provide<ITest>();
            //    if (instance != null)
            //        instance.Print();
            //}

            //Test0(new Locator());
            //Test1(new Locator());
        }

        //void Test0(Locator locator)
        //{
        //    try
        //    {
        //        locator.Bind<ITest, TestImplemetation1>();
        //        var instance = locator.Provide<ITest>();
        //        instance = locator.Provide<ITest>();
        //        instance = locator.Provide<ITest>();
        //        if (instance != null)
        //            instance.Print();

        //        Debug.Assert(instance != null);
        //    }
        //    catch { }
        //}

        //void Test1(Locator locator)
        //{
        //    try
        //    {
        //        locator.Bind<ITest, ITestImplemetation>();
        //        var instance = locator.Provide<ITest>();
        //        instance.Print();
        //        //Unable to pass interface type as implemetaation, so exception shoud be here - here we pass false durecty;
        //        Debug.Assert(false, "Exception expected");
        //    }
        //    catch { }
        //}
    }

    [FactoryOverride(typeof(CustomParamFactory), "TestFactoryParamValue")]
    public sealed class TestImplemetation0 : ITest
    {
#pragma warning disable 0649
        [Inject]
        public ITestRef FieldToInject { get; private set; }
#pragma warning restore 0649
        //void ITest.Print() => Console.WriteLine($"{GetType().FullName}  {GetHashCode()}");
        public void Print() => FieldToInject.Print();
    }

    public sealed class TestImplemetation1 : ITestRef
    {
//#pragma warning disable 0649
//        [Inject]
//        readonly ITest m_Ref;
//#pragma warning restore 0649

        void ITestRef.Print() => Console.WriteLine($"{GetType().FullName}  {GetHashCode()}");
    }

    [DefaultImplementation(typeof(TestImplemetation0), ImplementaionBehaviour.Singleton)]
    public interface ITest
    {
        public void Print();
    }

    [DefaultImplementation(typeof(TestImplemetation1), ImplementaionBehaviour.Singleton)]
    public interface ITestRef
    {
        public void Print();
    }

    public sealed class CustomFactory : InstanceFactory
    {
        readonly string m_SomeStringValue;
        public CustomFactory(Type implemetation, ImplementaionBehaviour behaviour, object[] args) : base(implemetation, behaviour, args) 
        {
            if (TryGetArgument<string>(0, out var someStringParam))
                m_SomeStringValue = someStringParam;
        }

        public override object Produse()
        {
            Console.WriteLine($"Factory {GetType().FullName} : param: {m_SomeStringValue}");
            return Activator.CreateInstance(Implemetation);
        }
    }

    public sealed class CustomParamFactory : InstanceFactory
    {
        readonly string m_SomeStringValue;

        public CustomParamFactory(Type implemetation, ImplementaionBehaviour behaviour, object[] args) : base(implemetation, behaviour, args) 
        {
            if (TryGetArgument<string>(0, out var someStringParam))
                m_SomeStringValue = someStringParam;
        }

        public override object Produse()
        {
            Console.WriteLine($"Factory {GetType().FullName} : {Implemetation.FullName} : param: {m_SomeStringValue}");
            return Activator.CreateInstance(Implemetation);
        }
    }
}

