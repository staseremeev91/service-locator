﻿using System;

namespace Core.IOC
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class LazySingletonProduseAttribute : Attribute
    {
        public LazySingletonProduseAttribute() { }
    }
}
