﻿using System;

namespace Core.IOC
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class FactoryOverrideAttribute : Attribute
    {
        public readonly Type FactoryType;
        public readonly object[] Args;

        public FactoryOverrideAttribute(Type factoryOverride, params object[] args)
        {
            FactoryType = factoryOverride;
            Args = args;
        }
    }
}
