﻿using System;

namespace Core.IOC
{
    public enum ImplementaionBehaviour
    {
        Transient,
        Singleton,
    }

    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class DefaultImplementationAttribute : Attribute
    {
        public readonly Type Implemetation;
        public readonly ImplementaionBehaviour Behaviour;

        public DefaultImplementationAttribute(Type implementaion, ImplementaionBehaviour behaviour = ImplementaionBehaviour.Transient)
        {
            Implemetation = implementaion;
            Behaviour = behaviour;
        }
    }
}
