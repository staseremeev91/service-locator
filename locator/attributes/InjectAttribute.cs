﻿using System;

namespace Core.IOC
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class InjectAttribute : Attribute
    {
        public InjectAttribute() { }
    }
}
