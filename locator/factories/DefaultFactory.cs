﻿using System;

namespace Core.IOC
{
    public sealed class DefaultFactory : InstanceFactory
    {
        public DefaultFactory(Type implemetation, ImplementaionBehaviour behaviour, object[] args) : base(implemetation, behaviour, args) { }

        public override object Produse() =>
             Activator.CreateInstance(Implemetation);
    }
}
