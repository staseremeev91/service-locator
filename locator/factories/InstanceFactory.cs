﻿using System;

namespace Core.IOC
{
    public abstract class InstanceFactory
    {
        public readonly Type Implemetation;
        public readonly ImplementaionBehaviour Behaviour;

        readonly object[] m_Args;

        public InstanceFactory(Type implemetation, ImplementaionBehaviour behaviour, object[] args)
        {
            Implemetation = implemetation;
            Behaviour = behaviour;

            m_Args = args;
        }

        public abstract object Produse();

        public bool TryGetArgument<TArgument>(int index, out TArgument argument)
        {
            argument = default;

            try
            {
                argument = (TArgument)m_Args[index];
                return true;
            }
            catch 
            {
                Console.WriteLine($"Unable to get parameter of type '{typeof(TArgument).Name}' at '{index}'");
            }

            return false;
        }
    }
}
