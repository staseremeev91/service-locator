﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Core.IOC
{
    public class Locator
    {
        readonly Dictionary<int, InstanceFactory> m_Factories;
        readonly Dictionary<int, object> m_Singletons;
        readonly Dictionary<int, int> m_InterfaceToImplementationMap;

        public Locator()
        {
            m_Factories = new Dictionary<int, InstanceFactory>();
            m_Singletons = new Dictionary<int, object>();
            m_InterfaceToImplementationMap = new Dictionary<int, int>();

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                foreach (var type in assembly.GetTypes())
                {
                    if (Attribute.GetCustomAttribute(type, typeof(DefaultImplementationAttribute)) is DefaultImplementationAttribute defaultImplementationAttribute)
                    {
                        var factoryType = typeof(DefaultFactory);
                        var factoryArgs = new object[] { };

                        if (Attribute.GetCustomAttribute(defaultImplementationAttribute.Implemetation, typeof(FactoryOverrideAttribute)) is FactoryOverrideAttribute factoryOverrideAttribute)
                        {
                            factoryType = factoryOverrideAttribute.FactoryType;
                            factoryArgs = factoryOverrideAttribute.Args;
                        }

                        Bind(type, defaultImplementationAttribute.Implemetation, factoryType, defaultImplementationAttribute.Behaviour, factoryArgs);
                    }
                }

            foreach (var factory in m_Factories.Values)
                if (factory.Behaviour == ImplementaionBehaviour.Singleton && !Attribute.IsDefined(factory.Implemetation, typeof(LazySingletonProduseAttribute)))
                    ProvideImplementation(factory.Implemetation.GetHashCode());
        }

        public TInterface Provide<TInterface>()
            where TInterface : class =>
                Provide(typeof(TInterface)) as TInterface;

        public void Bind<TInterface, TImplemetation>(ImplementaionBehaviour behaviour = ImplementaionBehaviour.Transient) 
            where TInterface : class
            where TImplemetation : class, TInterface =>
                Bind<TInterface, TImplemetation, DefaultFactory>(behaviour);

        public void Bind<TInterface, TImplemetation, TFactory>(ImplementaionBehaviour behaviour = ImplementaionBehaviour.Transient)
            where TInterface : class
            where TImplemetation : class, TInterface
            where TFactory : InstanceFactory =>
                Bind(typeof(TInterface), typeof(TImplemetation), typeof(TFactory), behaviour);

        public object Provide(Type interfaceType)
        {
            var interfaceTypeHash = interfaceType.GetHashCode();

            if (!m_InterfaceToImplementationMap.TryGetValue(interfaceTypeHash, out var implementationTypeHash))
                throw new InvalidOperationException();

            return ProvideImplementation(implementationTypeHash);
        }

        object ProvideImplementation(int implementationTypeHash)
        {
            if (!m_Factories.TryGetValue(implementationTypeHash, out var factory))
                throw new InvalidOperationException();

            if (factory.Behaviour == ImplementaionBehaviour.Singleton)
            {
                if (!m_Singletons.TryGetValue(implementationTypeHash, out var instance))
                    m_Singletons.Add(implementationTypeHash, instance = ProduseAndResolve(factory));

                return instance;
            }

            return ProduseAndResolve(factory);
        }

        #region PRIVATE
        object ProduseAndResolve(InstanceFactory factory)
        {
            var instance = factory.Produse();
            if (instance == null)
                throw new NullReferenceException();

            Resolve(instance);
            return instance;
        }

        void Resolve(object instance)
        {
            var instanceType = instance.GetType();
            var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

            foreach (var property in instanceType.GetProperties(flags))
            {
                if (Attribute.IsDefined(property, typeof(InjectAttribute)))
                {
                    var dependency = Provide(property.PropertyType);
                    property.SetValue(instance, dependency);
                }
            }

            foreach (var field in instanceType.GetFields(flags))
            {
                if (Attribute.IsDefined(field, typeof(InjectAttribute)))
                {
                    var dependency = Provide(field.FieldType);
                    field.SetValue(instance, dependency);
                }
            }
        }

        void Bind(Type interfaceType, Type implementationType, Type factoryType, ImplementaionBehaviour behaviour, params object[] factoryArgs)
        {
            var interfaceTypeHash = interfaceType.GetHashCode();

            if(!m_InterfaceToImplementationMap.ContainsKey(interfaceTypeHash))
            {
                var implemetationTypeHash = implementationType.GetHashCode();
                m_InterfaceToImplementationMap.Add(interfaceTypeHash, implemetationTypeHash);

                if (!m_Factories.ContainsKey(implemetationTypeHash))
                {
                    if (implementationType.IsInterface)
                        throw new InvalidOperationException($"Implementation of type '{implementationType.FullName}' couldn't be interface");

                    if (behaviour == ImplementaionBehaviour.Singleton && !implementationType.IsSealed)
                        throw new InvalidOperationException($"Implementation of type '{implementationType.FullName}' should be sealed it order to be a singleton");

                    if (!interfaceType.IsAssignableFrom(implementationType))
                        throw new InvalidOperationException($"Implementation of type '{implementationType.FullName}' should be inherited for '{interfaceType.FullName}'");

                    var factoryBaseType = typeof(InstanceFactory);
                    if (!factoryBaseType.IsAssignableFrom(factoryType))
                        throw new InvalidOperationException($"Factory of type '{factoryType.FullName}' should be inherited for '{factoryBaseType.FullName}'");

                    if (!(Activator.CreateInstance(factoryType, implementationType, behaviour, factoryArgs) is InstanceFactory factory))
                        throw new InvalidOperationException($"Unable to build factory for of type {factoryType.FullName}");

                    m_Factories.Add(implemetationTypeHash, factory);
                }
            }
            else
                throw new InvalidOperationException($"Interface {interfaceType.FullName} already registred for type {implementationType.FullName}");
           
        }
        #endregion
    }
}