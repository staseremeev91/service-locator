﻿using System;

namespace service_locator
{
    class Program
    {
        static void Main(string[] args)
        {
            new service_locator.locator.tests.LocatorTests().Start();
        }
    }
}
